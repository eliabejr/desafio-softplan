# Desafio implementador front-end: React, HTML, CSS & JS


### Protótipos
- [Design das páginas](https://www.figma.com/proto/BTa9Vpz4S1XUscURxANvFH5Z/DESAFIO?node-id=19%3A70&scaling=scale-down&redirected=1)
- [guideline](https://www.figma.com/proto/BTa9Vpz4S1XUscURxANvFH5Z/DESAFIO?node-id=27%3A1&scaling=contain&redirected=1) disponibilizados ao máximo


### Informações sobre desenvolvimento
- Projeto criado utilizando a biblioteca create-react-app.
- Foi utilizado o component pack [MaterialUI](https://material-ui.com/) e o pacote de ícones [react-icons](https://github.com/react-icons/react-icons). Ambos fornecem facilidade e permitiram que o layout ficasse fiel ao proposto.
- Optei por uma estilização mais enxuta, utilizando [styled-components](https://styled-components.com). Cada componente tem seus próprios estilos, evitando retrabalho no código.


### Considerações
Há algumas coisas a serem melhoradas como:
- Implementação real da API.
- Testes e2e.


##  Rodando o app

No diretório /app, rode:

`yarn start`

Executa o app em modo desenolvimento.
Se o navegador não abrir automaticamente, digite [http://localhost:3000](http://localhost:3000) para ver o projeto.

Após isso, execute o backend desenvolvido pela Sofplan
`docker run -p 3002:3002 gcpasquadproduto/softplan-desafio-frontend`


## Estrutura de pastas

```
app/
  README.md
  node_modules/
  package.json  
  public/
    index.html
    favicon.ico
  src/
    componentes/
      Error.js
      FormField.js
      Link.js
      ListItem.js
      Loading.js
      ModalDialog.js
      ProcessList.js
      ProjectLabel.js
      Subtitle.js
      Title.js
      VGroup.js
      Wrapper.js
    pages/
      App.js
      Error404.js
      Processo.js
      Search.js
    utils/
      constants.js
      theme.js
    index.css
    index.js
    registerServiceWorkers.js
```
