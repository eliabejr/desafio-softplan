import React from 'react'
import styled from 'styled-components'

import HGroup from './HGroup'

const FieldWrapper = styled.div`
  margin-top: ${props => props.marginTop || '10px'};
  margin-bottom: ${props => props.marginBottom || '10px'};
  margin-right: ${props => props.marginRight || '10px'};
  margin-left: ${props => props.marginLeft || '10px'};
  max-width: ${props => props.maxWidth};
  width: 100%;
`

const Input = styled.input`
  border: none;
  box-shadow: 0 5px 10px 0 rgba(0, 0, 0, 0.08);
  background: #fff;
  color: #252a34;
  font-family: -apple-system, system-ui, BlinkMacSystemFont, Arial, sans-serif;
  display: block;
  padding-top: 15px;
  padding-bottom: 15px;
  padding-left: 15px;
  padding-right: ${props => (props.withIcon ? '50px' : '15px')}
  outline: none;
  transition: all 0.2s;
  width: 100%;
  -webkit-appearance: none;
`

const Label = styled.label`
  color: #252a34;
  font-family: -apple-system, system-ui, BlinkMacSystemFont, Arial, sans-serif;
  font-weight: 400;
`

const HelperText = styled.span`
  color: #252a34;
  font-family: -apple-system, system-ui, BlinkMacSystemFont, Arial, sans-serif;
  font-size: 0.7rem;
  padding-left: 3px;
`

const Icon = styled.button`
  -webkit-appearance: none;
  background: none;
  border: none;
  margin-left: -50px;
  outline: none;
`

const FormField = ({
  name,
  type,
  label,
  placeholder,
  required,
  helperText,
  icon,
  marginBottom,
  marginLeft,
  marginRight,
  marginTop,
  maxWidth,
  onChange,
  value,
  withIcon,
}) => (
  <FieldWrapper
    marginBottom={marginBottom}
    marginLeft={marginLeft}
    marginRight={marginRight}
    marginTop={marginTop}
    maxWidth={maxWidth}
    withIcon={withIcon}
  >
    {!label ? null : <Label for={name}>{label}</Label>}
    <HGroup alignItems="center">
      <Input
        name={name}
        type={type}
        placeholder={placeholder}
        required={required}
        value={value}
        onChange={onChange}
      />
      {withIcon ? <Icon type="submit">{icon}</Icon> : null}
    </HGroup>
    {!helperText ? null : <HelperText>{helperText}</HelperText>}
  </FieldWrapper>
)

export default FormField
