import styled from 'styled-components'
import theme from '../utils/theme'

export default styled.a`
  color: ${theme.colors.primary};
  cursor: pointer;
  font-weight: 700;
  text-decoration: underline;
`
