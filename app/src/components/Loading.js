import React from 'react'
import styled, { keyframes } from 'styled-components'
import LoadingIcon from 'react-icons/lib/go/sync'

const spin = keyframes`
  from {
    transform: rotate(0deg);
  }
   to {
    transform: rotate(360deg);
  }
`

const LoadingWrapper = styled.div`
  animation: ${spin} 10s infinite linear;
  width: 48px;
  height: 48px;
`

const Loading = () => (
  <LoadingWrapper>
    <LoadingIcon/>
  </LoadingWrapper>
)

export default Loading