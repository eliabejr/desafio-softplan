import styled from 'styled-components'

export default styled.div`
  background: #fff;

  left: 50%;
  max-width: 600px;
  padding: 30px;
  position: fixed;
  top: 50%;
  transform: translate(-50%,-50%);
  transition: .6s ease-in-out;
  width: 80%;
  z-index: 103;
`
