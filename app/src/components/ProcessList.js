import React from 'react';
import Grid from '@material-ui/core/Grid'

const ProcessList = ({ children }) => (
  <Grid container spacing={24}>
    <Grid item xs={12} sm={0} md={1}></Grid>
    <Grid item xs={12} sm={12} md={12}>
      {children}
    </Grid>
  </Grid>
)


export default ProcessList