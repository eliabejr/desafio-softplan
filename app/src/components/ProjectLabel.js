import React from 'react'
import styled from 'styled-components'

import Subtitle from './Subtitle'

const Content = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`

const ProjectLabel = ({ title, children }) => (
  <div>
    <Subtitle>{title}</Subtitle>
    <Content>{children}</Content>
  </div>
)

export default ProjectLabel