import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import App from './pages/App'
import Search from './pages/Search'
import Process from './pages/Process'
import NotFound from './pages/404'
import registerServiceWorker from './registerServiceWorker'

import './index.css'

ReactDOM.render(
  <BrowserRouter>
    <Switch>
      <Route path="/" exact={true} component={App} />
      <Route path="/result/" component={Search} />
      <Route path="/processo/" component={Process} />
      <Route path='*' component={NotFound} />
    </Switch>
  </BrowserRouter>,
  document.getElementById('root')
)
registerServiceWorker()
