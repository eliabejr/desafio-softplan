import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import Wrapper from '../components/Wrapper'
import Title from '../components/Title'

const NotFoundWrapper = styled(Wrapper)`
  height: 100vh;
`

const NotFound = () => (
    <NotFoundWrapper alignItems="center" justifyContent="center">
      <Title>Página não encontrada</Title>
      <Link className="text-dark" to="/">Voltar ao início</Link>
    </NotFoundWrapper>
)

export default NotFound