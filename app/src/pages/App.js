import React from 'react'
import styled from 'styled-components'
import Modal from '@material-ui/core/Modal'
import Typography from '@material-ui/core/Typography'
import Search from 'react-icons/lib/go/search'

import Title from '../components/Title'
import FormField from '../components/FormField'
import Wrapper from '../components/Wrapper'
import ModalDialog from '../components/ModalDialog'
import Link from '../components/Link'

const IndexWrapper = styled.section`
  align-item: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100vh;
  width: 100%;
`

const Form = styled.form`
  display: block;
  margin: 0 auto;
  width: 100%;
`

class App extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      keyword: '',
      open: false,
    }
    this.handleKeywordChange = this.handleKeywordChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleKeywordChange(event) {
    this.setState({ keyword: event.target.value.toLowerCase() })
  }

  handleSubmit(event) {
    event.preventDefault()
    if (this.state.keyword !== '') {
      this.props.history.push('/result?key=' + this.state.keyword)
    }
  }

  handleOpen = () => {
    this.setState({ open: true })
  }

  handleClose = () => {
    this.setState({ open: false })
  }

  render() {
    return (
      <IndexWrapper>
        <Wrapper alignItems="center" justifyContent="center">
          <Title>Busca de processos</Title>
          <Form onSubmit={this.handleSubmit}>
            <FormField
              marginLeft="auto"
              marginRight="auto"
              marginBottom="30px"
              maxWidth="500px"
              type="text"
              value={this.state.keyword}
              onChange={this.handleKeywordChange}
              placeholder="Pesquise por uma informação do processo"
              withIcon={true}
              icon={<Search />}
            />
          </Form>
          <span>
            Você pode criar um novo processo{' '}
            <Link onClick={this.handleOpen}>clicando aqui.</Link>
          </span>
        </Wrapper>
        <Modal
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description"
          open={this.state.open}
          onClose={this.handleClose}
        >
          <ModalDialog>
            <Typography variant="title" id="modal-title">
              Text in a modal
            </Typography>
            <Typography variant="subheading" id="simple-modal-description">
              Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
            </Typography>
          </ModalDialog>
        </Modal>
      </IndexWrapper>
    )
  }
}
export default App
