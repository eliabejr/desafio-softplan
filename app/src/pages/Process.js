import React from 'react'
import styled from 'styled-components'
import Close from 'react-icons/lib/go/x'
import Edit from 'react-icons/lib/go/pencil'

import ProjectLabel from '../components/ProjectLabel'
import Loading from '../components/Loading'
import Error from '../components/Error'
import HGroup from '../components/HGroup'
import VGroup from '../components/VGroup'

const ProcessThumb = styled.div`
  width: 100%;
  height: 100%;
`

const Footer = styled.footer`
  clear: both;
  width: 100%;
`

const Process = ({
  process,
  interessados,
  clickClose,
  clickRemove,
  isLoading,
  error,
}) => (
  <section>
    {isLoading ? (
      <Loading />
    ) : !error ? (
      <div className="wrapper Card">
        <Close onClick={clickClose}/>
        <VGroup>
          <HGroup>
            <ProcessThumb>
              <img src={process.thumb} alt={process.assunto} />
            </ProcessThumb>
            <HGroup>
              <ProjectLabel
                title="Processo"
                classTitle="Label-title-small"
                classValue="Label-text"
              >
                {process.numero}
              </ProjectLabel>
              <ProjectLabel
                title="Data"
                classTitle="Label-title-small"
                classValue="Label-text"
              >
                {process.entrada}
              </ProjectLabel>
              <ProjectLabel
                title="Assunto"
                classTitle="Label-title-small"
                classValue="Label-text"
                className="column-24 p-0 p-t-10"
              >
                {process.assunto}
              </ProjectLabel>
            </HGroup>
          </HGroup>
          <HGroup>
            <ProjectLabel
              title="Interessados"
              classTitle="Label-title-small"
              classValue="Label-text-small"
              className="column-24 "
            >
              {interessados}
            </ProjectLabel>
          </HGroup>
          <HGroup>
            <ProjectLabel
              title="Descrição"
              classTitle="Label-title-small"
              classValue="Label-text-small"
              className="column-24 "
            >
              {process.descricao}
            </ProjectLabel>
          </HGroup>
        </VGroup>
        <Footer>
          <Close onClick={clickRemove}/>
          <Edit/>
        </Footer>
      </div>
    ) : (
      <Error>{error.message}</Error>
    )}
  </section>
)

export default Process
