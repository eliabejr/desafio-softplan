import React, { Component } from 'react'
import queryString from 'query-string'

import Subtitle from '../components/Subtitle'
import ListItem from '../components/ListItem'
import FormField from '../components/FormField'
import Loading from '../components/Loading'
import Process from './Process'

import constants from '../utils/constants'
import Wrapper from '../components/Wrapper'
import ProcessList from '../components/ProcessList';

class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      keyword: '',
      processos: [],
      isLoading: false,
      isLoadingDetail: false,
      selected: {},
      selectedIndex: null,
      selectedId: null,
      error: null,
      errorDetail: null,
      listInDetailMode: false,
    }
    this.handleKeywordChange = this.handleKeywordChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.fetchProcessList = this.fetchProcessList.bind(this)
  }

  handleKeywordChange = event => {
    this.setState({ keyword: event.target.value.toLowerCase() })
  }

  handleSubmit = event => {
    event.preventDefault()
    if (this.state.keyword !== '') {
      this.cleanSelection()
      this.setState({ processos: [], isLoading: true })
      this.props.history.push('/results?query=' + this.state.keyword)
      this.fetchProcessList()
    }
  }

  cleanSelection = () => {
    this.state.processos.map(function(item) {
      return (item.selected = false)
    })
    this.setState({
      listInDetailMode: false,
      selected: [],
    })
  }

  handleCloseDetail = () => {
    this.cleanSelection()
  }

  handleRemoveProcess = () => {
    this.setState({ isLoadingDetail: true })
    fetch(constants.processEndpoint + this.state.selected.id, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      method: 'DELETE',
    }).then(response => {
      if (response.ok) {
        this.setState({ isLoadingDetail: false })
        this.handleCloseDetail()
        this.fetchProcessList()
      } else {
        throw new Error(
          'Houve um problema na sua solicitação, por favor, tente novamente'
        )
      }
    })
  }

  handleSelectProcess = (processo, index) => {
    this.cleanSelection()

    let newProcessos = Object.assign({}, this.state)
    newProcessos.processos[index].selected = true

    //fetch API
    fetch(constants.processEndpoint + processo.id)
      .then(response => {
        if (response.ok) {
          this.setState({ isLoadingDetail: false })
          return response.json()
        } else {
          throw new Error(
            'Houve um problema na sua solicitação, por favor, tente novamente'
          )
        }
      })
      .then(data => this.setState({ selected: data, isLoadingDetail: false }))
      .catch(error =>
        this.setState({ errorDetail: error, isLoadingDetail: false })
      )

    this.setState({
      processos: newProcessos.processos,
      listInDetailMode: true,
      selectedIndex: index,
    })
  }

  fetchProcessList = keyword => {
    !keyword && !this.state.keyword ? this.props.history.push('/404') : this.setState({ isLoading: true })
    let q = this.state.keyword || keyword
    this.setState({ keyword: q })

    fetch(constants.searchEndpoint + q, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/jsonapplication/json;charset=UTF-8',
      },
      mode: 'cors',
    })
      .then(res => res.json())
      .then(data => {
        this.setState({
          processos: data,
          isLoading: false,
        })
      })
  }

  componentDidMount() {

    const values = queryString.parse(this.props.location.search)
    let path = this.props.location.pathname

    let url = path.split('/')
    if (url[1] === 'processo') {
      this.setState({ selectedId: url[2] })
    }
    if (url[1] === 'result') {
      if (values.key) {
        this.fetchProcessList(values.key)
      } else {
        this.props.history.push('/')
      }
    }
  }

  render() {
    const {
      selected,
      selectedIndex,
      processos,
      isLoading,
      errorDetail,
      isLoadingDetail,
      listInDetailMode,
    } = this.state

    return (
      <Wrapper>
        <FormField
          type="text"
          value={this.state.keyword}
          onChange={this.handleKeywordChange}
          placeholder="Pesquise por uma informação do processo"
          withIcon={true}
          icon={<Search />}
        />
        {isLoading ? (
          <Loading />
        ) : (
          <div>
            {processos.length > 0 ? (
              <ProcessList>
                {processos.map((processo, index) => (
                  <ListItem
                    key={processo.id}
                    processo={processo}
                    compact={listInDetailMode}
                    onClick={this.handleSelectProcess.bind(
                      this,
                      processo,
                      index
                    )}
                  />
                ))}
                {selected ? (
                  <Process
                    process={selected}
                    index={selectedIndex}
                    interessados={selected.interessados}
                    clickClose={this.handleCloseDetail.bind(this)}
                    clickRemove={this.handleRemoveProcess.bind(this)}
                    error={errorDetail}
                    isLoading={isLoadingDetail}
                  />
                ) : null}
              </ProcessList>
            ) : (
              <Subtitle>Nenhum registro encontrado</Subtitle>
            )}
          </div>
        )}
      </Wrapper>
    )
  }
}

export default Search
