const constants = {
  endpoint: process.env.endpoint || 'http://localhost:3002/',
  searchEndpoint: process.env.endpoint || 'http://localhost:3002/processo/?search=',
  processEndpoint: process.env.endpoint || 'http://localhost:3002/processo/'
}

export default constants
