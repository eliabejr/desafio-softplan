const theme = {
  fonts: {
    default: 'Montserrat, sans-serif',
  },
  colors: {
    primary: '#005b95',
    black87: '#212121',
    black54: '#757575',
    black38: '#9E9E9E',
  },
}

export default theme
